<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 10:54 AM
 */

namespace Smorken\Repositories\Storage\Contracts;


use Smorken\Repositories\Filter\Filter;
use Smorken\Repositories\Model\Contracts\Crud as MCrud;

interface Crud extends Repo {

    /**
     * @param MCrud $model
     * @param array|null $rules
     * @return boolean
     */
    public function validate($model, $rules = null);

    /**
     * Find a single entity
     *
     * @param int $id
     * @param array $criteria
     * @return MCrud|null
     */
    public function find($id, array $criteria = array());

    /**
     * Find the first entity matching criteria
     *
     * @param array $criteria
     * @return MCrud|null
     */
    public function first(array $criteria = array());

    /**
     * @param array $criteria
     * @return MCrud[]
     */
    public function all(array $criteria = array());

    /**
     * @param Filter $filter
     * @param array $criteria
     * @return mixed
     */
    public function filtered(Filter $filter, array $criteria = array(), $perPage = 0);

    /**
     * Create a new entity
     *
     * @param array $input
     * @return MCrud
     */
    public function create(array $input);

    /**
     * Update an existing entity
     *
     * @param MCrud $model
     * @param array $input
     * @return MCrud
     */
    public function update($model, array $input);

    /**
     * Delete an existing entity
     *
     * @param MCrud $model
     * @return boolean
     */
    public function delete($model);

    /**
     * Delete several models by id(s)
     * @param mixed $ids
     * @return mixed
     */
    public function destroy($ids);
}