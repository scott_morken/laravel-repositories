<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 11:01 AM
 */

namespace Smorken\Repositories\Storage\Contracts;


interface Pageable extends Repo {

    /**
     * Get Results by Page
     *
     * @param int $perPage
     * @param array $criteria
     * @return mixed
     */
    public function paginate($perPage = 15, $criteria = array());
}