<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 11:47 AM
 */

namespace Smorken\Repositories\Storage\Contracts;


use Smorken\Repositories\Model\Contracts\Model;

interface Repo {

    /**
     * @return Model
     */
    public function getModel();

    /**
     * @param Model
     */
    public function setModel($model);

    /**
     * Return  a string representing the name
     * @param Model $model
     * @return mixed
     */
    public function name($model);

    /**
     * Return the model's id
     * @param Model $model
     * @return mixed
     */
    public function id($model);

    /**
     * @return \Illuminate\Support\MessageBag|false
     */
    public function errors();

    public function addError($key, $value);
}