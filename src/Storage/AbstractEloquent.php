<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 11:31 AM
 */

namespace Smorken\Repositories\Storage;

use Smorken\Repositories\Storage\Traits\Crud;
use Smorken\Repositories\Storage\Traits\Pageable;

class AbstractEloquent extends AbstractBase {

    use Crud, Pageable;

}