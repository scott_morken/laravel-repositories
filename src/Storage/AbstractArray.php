<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 7:50 AM
 */

namespace Smorken\Repositories\Storage;


use Smorken\Repositories\Storage\Traits\Crud;

class AbstractArray extends AbstractBase {

    use Crud;

    /**
     * Find a single entity
     *
     * @param int $id
     * @param array $criteria
     * @return MCrud|null
     */
    public function find($id, array $criteria = array())
    {
        return array_get($this->getModel(), $id);
    }

    /**
     * Find the first entity matching criteria
     *
     * @param array $criteria
     * @return MCrud|null
     */
    public function first(array $criteria = array())
    {
        if (isset($criteria['where'])) {
            $array = array_get($this->getModel(), $criteria['where']);
        }
        else {
            $array = $this->getModel();
        }
        return head($array);
    }

    /**
     * @param array $criteria
     * @return MCrud[]
     */
    public function all(array $criteria = array())
    {
        return $this->getModel();
    }

    /**
     * Create a new entity
     *
     * @param array $input
     * @return MCrud
     */
    public function create(array $input)
    {
        if (isset($input['key']) && isset($input['value'])) {
            array_set($this->getModel(), $input['key'], $input['value']);
        }
    }

    /**
     * Update an existing entity
     *
     * @param MCrud $model
     * @param array $input
     * @return MCrud
     */
    public function update($model, array $input)
    {
        $this->create($input);
    }

    /**
     * Delete an existing entity
     *
     * @param MCrud $model
     * @return boolean
     */
    public function delete($model)
    {
        foreach($this->getModel() as $k => $v) {
            if ($v == $model) {
                unset($this->getModel()[$k]);
            }
        }
    }

}