<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 12:50 PM
 */

namespace Smorken\Repositories\Storage;


use Smorken\Repositories\Storage\Traits\Crud;
use Smorken\Repositories\Storage\Traits\Pageable;
use Illuminate\Contracts\Config\Repository as Config;

class AbstractConfig extends AbstractBase {

    use Crud, Pageable;

    /**
     * @var Config
     */
    protected $config;

    public function __construct($model, Config $config)
    {
        $this->setConfig($config);
        parent::__construct($model);
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Find a single entity
     *
     * @param int $id
     * @param array $criteria
     * @return MCrud|null
     */
    public function find($id, array $criteria = array())
    {
        return $this->getConfig()->get($id);
    }

    /**
     * Find the first entity matching criteria
     *
     * @param array $criteria
     * @return MCrud|null
     */
    public function first(array $criteria = array())
    {
        $query = $this->make($criteria);
        return head($query);
    }

    /**
     * @param array $criteria
     * @return MCrud[]|Model[]
     */
    public function all(array $criteria = array())
    {
        return $this->make($criteria);
    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $criteria
     * @return Query
     */
    public function make(array $criteria = array())
    {
        if (isset($criteria['config'])) {
            return $this->getConfig()->get($criteria['config']);
        }
        return $this->getConfig()->get();
    }

    /**
     * Create a new entity
     *
     * @param array $input
     * @return MCrud
     */
    public function create(array $input)
    {
        $key = $input['key'];
        $value = $input['value'];
        $this->getConfig()->set($key, $value);
    }

    /**
     * Update an existing entity
     *
     * @param MCrud|Model $model
     * @param array $input
     * @return MCrud
     */
    public function update($model, array $input)
    {
        $this->create($input);
    }

    /**
     * Delete an existing entity
     *
     * @param MCrud $model
     * @return boolean
     */
    public function delete($model)
    {
        $this->getConfig()->set($model['key'], null);
    }
}