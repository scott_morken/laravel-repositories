<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 11:46 AM
 */

namespace Smorken\Repositories\Storage;

use Smorken\Repositories\Model\Contracts\Model;
use Smorken\Repositories\Model\Contracts\Query;

abstract class AbstractBase {

    public static $defaultCriteria = array();

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct($model)
    {
        $this->setModel($model);
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Model $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @param Model $model
     * @return mixed
     */
    public function name($model)
    {
        return $model->name();
    }

    /**
     * @param Model $model
     * @return mixed
     */
    public function id($model)
    {
        return $model->getKey();
    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $criteria
     * @return Query
     */
    public function make(array $criteria = array())
    {
        if (empty($criteria)) {
            $criteria = static::$defaultCriteria;
        }
        $query = $this->model->newQuery();
        foreach($criteria as $what => $items) {
            $this->addCriteria($what, $items, $query);
        }
        return $query;
    }

    /**
     * @param $what
     * @param $items
     * @param Query $query
     * @return mixed
     */
    protected function addCriteria($what, $items, $query)
    {
        if (!$items) {
            return call_user_func(array($query, $what));
        }
        foreach($items as $item) {
            if (is_array($item)) {
                call_user_func_array(array($query, $what), $item);
            }
            else {
                if ($item) {
                    call_user_func(array($query, $what), $item);
                }
                else {
                    call_user_func(array($query, $what));
                }
            }
        }
    }

    /**
     * Passes through missing methods to the model
     * @param $name
     * @param $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        return call_user_func_array(array($this->model, $name), $args);
    }
}