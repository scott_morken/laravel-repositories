<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 10:21 AM
 */

namespace Smorken\Repositories\Storage\Traits;

use Smorken\Repositories\Filter\Filter;
use Smorken\Repositories\Model\Contracts\Crud as MCrud;
use Smorken\Repositories\Model\Contracts\Model;

trait Crud {

    /**
     * @var \Illuminate\Support\MessageBag
     */
    protected $errors;

    /**
     * Return the errors
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function errors()
    {
        if (!$this->errors) {
            $this->errors = new \Illuminate\Support\MessageBag();
        }
        if (!$this->errors->isEmpty()) {
            return $this->errors;
        }
        return false;
    }

    public function addError($key, $value)
    {
        if (!$this->errors) {
            $this->errors = new \Illuminate\Support\MessageBag();
        }
        $this->errors->add($key, $value);
    }

    /**
     * Find a single entity
     *
     * @param int $id
     * @param array $criteria
     * @return MCrud|null
     */
    public function find($id, array $criteria = array())
    {
        $query = $this->make($criteria);
        return $query->find($id);
    }

    /**
     * Find the first entity matching criteria
     *
     * @param array $criteria
     * @return MCrud|null
     */
    public function first(array $criteria = array())
    {
        $query = $this->make($criteria);
        return $query->first();
    }

    /**
     * @param array $criteria
     * @return MCrud[]|Model[]
     */
    public function all(array $criteria = array())
    {
        $query = $this->make($criteria);
        return $query->get();
    }

    /**
     * @param Filter $filter
     * @param array $criteria
     * @return MCrud[]|Model[]
     */
    public function filtered(Filter $filter, array $criteria = array(), $perPage = 0)
    {
        $query = $this->getFilterQuery($filter, $criteria);
        if ($perPage === 0) {
            return $query->get();
        }
        else {
            return $query->paginate($perPage);
        }
    }

    protected function getFilterQuery($filter, $criteria)
    {
        $query = $this->make($criteria);
        foreach($filter->toArray() as $scope => $value) {
            $query->$scope($value);
        }
        return $query;
    }

    /**
     * Create a new entity
     *
     * @param array $input
     * @return MCrud
     */
    public function create(array $input)
    {
        $model = $this->model->newInstance($input);
        if ($this->validate($model, $this->rules())) {
            if ($model->save()) {
                return $model;
            }
        }
        return false;
    }

    /**
     * Update an existing entity
     *
     * @param MCrud|Model $model
     * @param array $input
     * @return MCrud
     */
    public function update($model, array $input)
    {
        $model->fill($input);
        if ($this->validate($model, $this->rules())) {
            if ($model->save()) {
                return $model;
            }
        }
        return false;
    }

    /**
     * Delete an existing entity
     *
     * @param MCrud $model
     * @return boolean
     */
    public function delete($model)
    {
        if (is_string($model)) {
            $model = $this->find($model);
        }
        if ($model) {
            return $model->delete();
        }
        return false;
    }

    /**
     * @param $ids
     */
    public function destroy($ids)
    {
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach($ids as $id) {
            $model = $this->find($id);
            if ($model) {
                $model->delete();
            }
        }
    }

    /**
     * @param MCrud $model
     * @param array|null $rules
     * @return boolean
     */
    public function validate($model, $rules = null)
    {
        if ($rules === null) {
            $rules = $model->rules();
        }
        if ($rules) {
            $validator = $this->getValidationFactory()->make($model->getAttributes(), $rules);
            if ($validator->fails()) {
                $this->errors = $validator->messages();
                return false;
            }
        }
        return true;
    }

    /**
     * Get a validation factory instance.
     *
     * @return \Illuminate\Contracts\Validation\Factory
     */
    protected function getValidationFactory()
    {
        return \app('Illuminate\Contracts\Validation\Factory');
    }
}