<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 11:24 AM
 */

namespace Smorken\Repositories\Storage\Traits;


trait Pageable {

    /**
     * Get Results by Page
     *
     * @param int $perPage
     * @param array $criteria
     * @return mixed
     */
    public function paginate($perPage = 15, $criteria = array())
    {
        $query = $this->make($criteria);
        return $query->paginate($perPage);
    }
}