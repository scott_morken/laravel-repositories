<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 11:31 AM
 */

namespace Smorken\Repositories\Storage;


use Illuminate\Filesystem\Filesystem;
use Smorken\Repositories\Model\VO\File;
use Smorken\Repositories\RepositoryException;
use Smorken\Repositories\Storage\Traits\Crud;
use Smorken\Repositories\Storage\Traits\Pageable;

class AbstractFile extends AbstractBase {

    use Crud, Pageable;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    public function __construct($model, Filesystem $filesystem)
    {
        $this->setFilesystem($filesystem);
        parent::__construct($model);
    }

    public function getFilesystem()
    {
        return $this->filesystem;
    }

    public function setFilesystem(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function all(array $criteria = array())
    {
        $files = array();
        foreach ($this->getPaths($criteria) as $path) {
            $files = array_merge($files, $this->loadFilesInPath($path));
        }
        return $files;
    }

    /**
     * Find a single entity
     *
     * @param string $id
     * @param array $criteria
     * @return File
     */
    public function find($id, array $criteria = array())
    {
        $file = null;
        foreach($this->getPaths($criteria) as $path) {
            $temp = $this->loadFilesInPath($path, $id);
            if ($temp) {
                $file = $temp;
                break;
            }
        }
        $this->setModel($file);
        return $file;
    }

    public function first(array $criteria = array())
    {
        $file = null;
        foreach($this->getPaths($criteria) as $path) {
            $tempfiles = $this->loadFilesInPath($path);
            if ($tempfiles) {
                $file = reset($tempfiles);
                break;
            }
        }
        $this->setModel($file);
        return $file;
    }

    public function update($model, array $input)
    {
        $model->fill($input);
        if ($this->validate($model, $model->rules())) {
            return $model->save();
        }
        return false;
    }

    public function create(array $input)
    {
        $model = $this->model->fileNewInstance($this->getFilesystem());
        $model->fill($input);
        if ($this->validate($model, $model->rules())) {
            return $model->save();
        }
        return false;
    }

    public function delete($model)
    {
        return $model->delete();
    }

    public function destroy($ids)
    {

    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $criteria
     * @return array paths
     */
    public function make(array $criteria = array())
    {
        return $this->getPaths($criteria);
    }

    protected function getPaths($criteria = array())
    {
        $paths = array();
        if (!isset($criteria['where'])) {
            throw new RepositoryException("A path must be specified in the 'where' criteria");
        }
        foreach ($criteria['where'] as $wArray) {
            if (!is_array($wArray)) {
                throw new RepositoryException("Paths must be an array of arrays ('where' => array(array('path1'), array('path2')).");
            }
            $paths[] = $wArray[0];
        }
        return $paths;
    }

    protected function loadFilesInPath($path, $id = null)
    {
        $files = array();
        $f = $this->getFilesystem()->allFiles($path);
        foreach ($f as $file) {
            $class = $this->getModel()->fileNewInstance($this->getFilesystem());
            $obj = new $class($this->getFilesystem());
            $name = substr($file, strlen($path));
            $obj->name = $name;
            $obj->path = $path;
            $cid = $obj->createId();
            $obj->id = $cid;
            $files[$cid] = $obj;
            if ($id) {
                if ($id == $cid) {
                    return $obj;
                }
            }
        }
        if ($id) {
            return false;
        }
        return $files;
    }
}