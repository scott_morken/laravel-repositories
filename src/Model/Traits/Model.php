<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/6/15
 * Time: 7:06 AM
 */

namespace Smorken\Repositories\Model\Traits;


trait Model {

    abstract public function __toString();

    abstract public function name();

    abstract public function getKey();

    public function rules()
    {
        if (!isset($this->rules)) {
            $this->rules = array();
        }
        return $this->rules;
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }
}