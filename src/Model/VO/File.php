<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/7/14
 * Time: 11:50 AM
 */

namespace Smorken\Repositories\Model\VO;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Smorken\Repositories\Model\Contracts\Crud;
use Smorken\Repositories\Model\Contracts\Model;

/**
 * Class File
 * @package Smorken\Repositories\VO
 * @property string $id
 * @property string $name
 * @property string $path
 * @property mixed $data
 */
class File implements Model, Crud {

    protected $rules = array(
        'id' => 'required',
        'name' => 'required|min:1|max:128',
    );

    protected $ext = '.blade.php';

    protected $attributes = array(
        'id' => null,
        'name' => null,
        'path' => null,
        'data' => null,
    );

    protected $originalAttributes;

    /**
     * @var Filesystem
     */
    protected $backend;

    public function __construct(Filesystem $backend, $ext = null)
    {
        if ($ext) {
            $this->ext = Str::startsWith('.', $ext) ? $ext : '.'. $ext;
        }
        $this->backend = $backend;
        $this->originalAttributes = $this->attributes;
    }

    public function newInstance($attributes = array(), $exists = false)
    {

    }

    public function fileNewInstance($backend, $ext = null)
    {
        return new static($backend, $ext);
    }

    public function newQuery() {
        return true;
    }

    public function __toString()
    {
        return $this->name();
    }

    public function name()
    {
        return $this->attributes['name'];
    }

    public function toArray()
    {
        return $this->getAttributes();
    }

    public function getAttribute($key)
    {
        $methodname = camel_case($key);
        if (method_exists($this, $methodname)) {
            return $this->$methodname();
        }
        return array_get($this->attributes, $key, null);
    }

    public function setAttribute($key, $value)
    {
        $methodname = 'set' . studly_case($key);
        if (method_exists($this, $methodname)) {
            $this->$methodname($value);
        }
        else {
            $this->attributes[$key] = $value;
        }
    }

    public function getExtension()
    {
        return $this->ext;
    }

    public function reset()
    {
        $this->attributes = $this->originalAttributes;
    }

    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    public function __isset($key)
    {
        return array_key_exists($key, $this->attributes);
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes($attributes)
    {
        foreach($attributes as $key => $value) {
            $this->$key = $value;
        }
        if (!$this->attributes['id']) {
            $this->id = $this->createId();
        }
    }

    public function fill(array $attributes)
    {
        $this->setAttributes($attributes);
    }

    public function save(array $options = array())
    {
        if ($this->backend->put($this->fullPath(), $this->data)) {
            return $this;
        }
        return false;
    }

    public function createId()
    {
        return md5($this->fullPath());
    }

    public function getKey()
    {
        return $this->id;
    }

    public function setName($value)
    {
        $name = $value;
        if (ends_with($value, $this->ext)) {
            $name = substr($value, 0, - (strlen($this->ext)));
        }
        if (starts_with($name, '/')) {
            $name = substr($name, 1);
        }
        $name = Str::slug($name, '_');
        $this->attributes['name'] = Str::finish($name, $this->ext);
    }

    public function friendlyName()
    {
        $name = $this->name;
        if (ends_with($name, $this->ext)) {
            $name = substr($name, 0, strlen($name) - strlen($this->ext));
        }
        return ucwords(str_replace(array(DIRECTORY_SEPARATOR, '_', '-'), ' ', $name));
    }

    public function fullPath()
    {
        $path = $this->path;
        $path = Str::finish($path, DIRECTORY_SEPARATOR);
        $combined = $path . $this->name;
        return $combined;
    }

    /**
     * allows lazy loading file data
     * @return string
     * @throws VOException
     */
    public function data()
    {
        if (!$this->id) {
            throw new VOException("Cannot retrieve file data with a valid id");
        }
        if ($this->attributes['data']) {
            return $this->attributes['data'];
        }
        $fullpath = $this->fullPath();
        return $this->backend->get($fullpath);
    }

    /**
     * @param array $rules
     */
    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * Returns an array of rules to be used for validating
     * the model
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public static function find($id, $columns = array('*'))
    {
        // TODO: Implement find() method.
    }

    public static function all($columns = array('*'))
    {
        // TODO: Implement all() method.
    }

    public function delete()
    {
        return $this->backend->delete($this->fullPath());
    }

    public static function destroy($ids)
    {
        // TODO: Implement destroy() method.
    }
}