<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 10:23 AM
 */

namespace Smorken\Repositories\Model\Contracts;


interface Model {

    public function newInstance($attributes = array(), $exists = false);

    public function newQuery();

    public function __toString();

    public function getKey();

    public function name();

    public function toArray();

    public function getAttribute($key);

    public function setAttribute($key, $value);

    public function getAttributes();

    public function fill(array $attributes);

    /**
     * @param array $rules
     */
    public function setRules(array $rules);

    /**
     * Returns an array of rules to be used for validating
     * the model
     * @return array
     */
    public function rules();

}