<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 11:57 AM
 */

namespace Smorken\Repositories\Model\Contracts;


interface Query {

    public function paginate($perPage = 15, $columns = array('*'));

    public function get($columns = array('*'));

    public function find($id, $columns = array('*'));

    public function first($columns = array('*'));

    public function chunk($count, callable $callback);

    public function lists($column, $key = null);

    public function exists();

}