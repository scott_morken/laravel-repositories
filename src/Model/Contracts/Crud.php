<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 10:42 AM
 */

namespace Smorken\Repositories\Model\Contracts;


interface Crud extends Model {

    public static function all($columns = array('*'));

    public static function destroy($ids);

    public function newInstance($attributes = array(), $exists = false);

    public function save(array $options = array());

    public function delete();

    public function fill(array $attributes);

}