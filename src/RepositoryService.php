<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/7/14
 * Time: 12:22 PM
 */

namespace Smorken\Repositories;

class RepositoryService {

    public function load($name)
    {
        $repo = \app($name);
        return $repo;
    }

    public function __call($name, $args)
    {
        $repo = $this->load($args[0]);
        unset($args[0]);
        return call_user_func_array(array($repo, $name), $args);
    }

} 