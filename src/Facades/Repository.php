<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/7/14
 * Time: 12:23 PM
 */

namespace Smorken\Repositories\Facades;

use Illuminate\Support\Facades\Facade;

class Repository extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'smorken.repository'; }

} 