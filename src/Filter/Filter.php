<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 10:28 AM
 */

namespace Smorken\Repositories\Filter;


class Filter {

    protected $filters = array();

    protected $skipToArray = array();

    public function __construct($filters)
    {
        $this->filters = $filters;
    }

    public function __set($name, $value)
    {
        $this->filters[$name] = $value;
    }

    public function __get($name)
    {
        if (isset($this->filters[$name])) {
            return $this->filters[$name];
        }
        return null;
    }

    public function toArray($skip = true, $addSkip = array())
    {
        $array = array();
        foreach($this->filters as $k => $v) {
            if (!$skip || (!in_array($k, $this->skipToArray) && !in_array($k, $addSkip))) {
                $array[$k] = $v;
            }
        }
        return $array;
    }

    public static function create($filter_vars = array(), $name = null)
    {
        if ($name === null) {
            $name = get_called_class();
        }
        $filter = \Session::get($name, null);
        $resave = $filter === null;
        foreach($filter_vars as $key => $value) {
            if ($filter && $value !== $filter->$key) {
                $resave = true;
            }
        }
        if ($resave) {
            $filter = new static($filter_vars);
            \Session::put($name, $filter);
        }
        return $filter;
    }
}