<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/7/14
 * Time: 12:24 PM
 */

use Mockery as m;
use Smorken\Repositories\Model\VO\File;

class FileTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Repositories\Model\VO\File
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->fs = m::mock('\Illuminate\Contracts\Filesystem\Filesystem');

        $this->sut = new File($this->fs);
    }

    public function tearDown()
    {
        m::close();
    }

    public function testSetAttributes()
    {
        $attributes = array(
            'name' => 'file.blade.php',
            'path' => 'full/path/to/',
        );

        $this->sut->setAttributes($attributes);
        $attributes['data'] = null;
        $attributes['id'] = '5e2074e3555de3b15cc03ca0daff0c0a';
        $this->assertEquals($attributes, $this->sut->getAttributes());
    }

    public function testFriendlyNameWithExt()
    {
        $name = 'file_name.blade.php';
        $this->sut->name = $name;
        $this->assertEquals('File Name', $this->sut->friendlyName());
    }

    public function testNameFromFriendly()
    {
        $name = 'My File';
        $this->sut->name = $name;
        $this->assertEquals('my_file.blade.php', $this->sut->name);
    }

    public function testGetPreloadedData()
    {
        $data = array(
            'id' => 'some/file/path.php',
            'data' => 'some data',
        );
        $this->sut->setAttributes($data);
        $this->assertEquals('some data', $this->sut->data);
    }

    public function testLazyLoadedData()
    {
        $data = array(
            'id' => 'bar',
            'name' => 'path.blade.php',
            'path' => 'some/file/',
        );
        $this->sut->setAttributes($data);
        $this->fs->shouldReceive('get')
            ->once()
            ->with('some/file/path.blade.php')
            ->andReturn('foo');
        $this->assertEquals('foo', $this->sut->data);
    }
} 