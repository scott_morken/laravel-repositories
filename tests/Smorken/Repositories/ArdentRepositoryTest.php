<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/20/14
 * Time: 7:38 AM
 */

use Mockery as m;

class ArdentRepoTestCase extends \Smorken\Repositories\AbstractArdentRepository {
    use Smorken\Repositories\Traits\Eloquent\Crud;
    use Smorken\Repositories\Traits\Eloquent\Pageable;

}

class ArdentRepositoryTest extends PHPUnit_Framework_TestCase {
    /**
     * @var ArdentRepoTestCase
     */
    protected $sut;

    /**
     * @var Mockery/Mock
     */
    protected $model;

    public function setUp()
    {
        parent::setUp();
        $this->model = m::mock('LaravelBook\Ardent\Ardent');

        $this->sut = new ArdentRepoTestCase($this->model);
    }

    public function tearDown()
    {
        m::close();
    }

    public function testAddCriteria()
    {
        $criteria = array(
            'where' => array('test', '=', 1)
        );
        $query = m::mock('StdClass');
        $query->shouldReceive('where')
            ->with("test", "=", 1)
            ->once()
            ->andReturn($query);
        $this->model->shouldReceive('newQuery')
            ->andReturn($query);

        $model = $this->sut->make($criteria);
        $this->assertInstanceOf('StdClass', $model);
    }

    public function testAll()
    {
        $mockAll = m::mock('\Illuminate\Database\Eloquent\Collection');
        $query = m::mock('StdClass');
        $query->shouldReceive('get')
            ->once()
            ->andReturn($mockAll);
        $this->model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $this->assertInstanceOf('\Illuminate\Database\Eloquent\Collection', $this->sut->all());
    }

    public function testFind()
    {
        $id = 1;
        $model = m::mock('LaravelBook\Ardent\Ardent');
        $query = m::mock('StdClass');
        $query->shouldReceive('find')
            ->once()
            ->with($id)
            ->andReturn($model);
        $this->model->shouldReceive('newQuery')
            ->andReturn($query);
        $this->assertInstanceOf('LaravelBook\Ardent\Ardent', $this->sut->find($id));
    }

    public function testGetByPage()
    {
        $p = 2;
        $l = 15;
        $with = array();
        $resultArray = array(
            'page' => $p,
            'limit' => $l,
            'totalItems' => 100,
            'items' => array(),
        );
        $mockAll = m::mock('\Illuminate\Database\Eloquent\Collection');
        $mockAll->shouldReceive('all')
            ->once()
            ->andReturn(array());
        $query = m::mock('StdClass');
        $query->shouldReceive('skip->take->get')
            ->once()
            ->andReturn($mockAll);
        $this->model->shouldReceive('count')
            ->once()
            ->andReturn(100);
        $this->model->shouldReceive('newQuery')
            ->andReturn($query);
        $page = $this->sut->getByPage($p, $l, $with);

        $this->assertEquals($resultArray, (array) $page);
    }

    public function testGetBy()
    {
        $key = 'column';
        $value = 1;
        $with = array();
        $mockAll = m::mock('\Illuminate\Database\Eloquent\Collection');
        $this->model->shouldReceive('newQuery')
            ->andReturn($this->model);
        $this->model->shouldReceive('where')
            ->with($key, '=', $value)
            ->andReturn($this->model);
        $this->model->shouldReceive('get')
            ->andReturn($mockAll);
        $this->assertInstanceOf('\Illuminate\Database\Eloquent\Collection', $this->sut->getBy($key, $value, $with));
    }

    public function testErrors()
    {
        $this->model->shouldReceive('errors')
            ->andReturn(array());
        $this->assertFalse($this->sut->errors());
    }

    public function testValidate()
    {
        $this->model->shouldReceive('validate')
            ->andReturn(true);
        $this->assertEquals(true, $this->sut->validate());
    }

}